FROM surnet/alpine-python-wkhtmltopdf:3.6.5-0.12.5-small
LABEL maintainer="matthew.bednarski@aicomp.com"

# Install dependencies for running web service
RUN pip install --upgrade pip
RUN pip install werkzeug
RUN pip install executor
RUN pip install gunicorn
RUN pip install httplib2

RUN apk --no-cache add bash

ADD app.py /app/app.py

WORKDIR /app

EXPOSE 80

ENTRYPOINT ["/usr/local/bin/gunicorn"]

# Show the extended help
CMD ["-b", "0.0.0.0:80", "--log-file", "-", "app:application"]
