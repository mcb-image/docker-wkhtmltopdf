#! /usr/bin/env python
"""
    WSGI APP to convert wkhtmltopdf As a webservice

    :copyright: (c) 2013 by Openlabs Technologies & Consulting (P) Limited
    :license: BSD, see LICENSE for more details.
"""
import json
import tempfile
import pprint
import base64
import binascii


from urllib import parse
from werkzeug.wsgi import wrap_file
from werkzeug.wrappers import Request, Response
from werkzeug.datastructures import Headers
from executor import execute

pp = pprint.PrettyPrinter(indent = 4)

@Request.application
def application(request):
    """
    To use this application, the user must send a POST request with
    base64 or form encoded encoded HTML content and the wkhtmltopdf Options in
    request data, with keys 'base64_html' and 'options'.
    The application will return a response with the PDF file.
    """
    if request.method == 'POST':
        return handle_POST(request)
    elif request.method == 'GET':
        return handle_GET(request)
    else:
        return


def handle_GET(request):
    """
    To use this method, the user must send a POST request with
    base64 or form encoded encoded HTML content and the wkhtmltopdf Options in
    request data, with keys 'base64_html' and 'options'.
    The application will return a response with the PDF file.
    """
    if request.method != 'GET':
        return

    name = request.args.get('name', 'download.pdf')
    data = parse_request_data_get( request )

    if data :
        file_name = create_pdf( data["data"], name, data["options"])
        return handle_file_return(file_name, request)
    else:
        return Response(
            "Could not retriev 'data' contents", status = 400
        )


def parse_request_data_get( request ):
    """
    retrieves the content data using different strategies
    """
    data = None
    options = base64_decode(request.args.get('options', '{}'))
    if 'data' in request.args:
        data = base64_decode( request.args['data'] )
        print('decoding')

    if is_url( data ):
        # download url data
        data = fetch_data( data )

    return {
            "data": data,
            "options": options
    }

def handle_POST(request):
    """
    To use this method, the user must send a POST request with
    base64 or form encoded encoded HTML content and the wkhtmltopdf Options in
    request data, with keys 'base64_html' and 'options'.
    The application will return a response with the PDF file.
    """
    if request.method != 'POST':
        return

    name = request.form.get('name', 'download.pdf')
    data = parse_request_data_post( request )

    if data :
        file_name = create_pdf( data["data"], name, data["options"])
        return handle_file_return(file_name, request)
    else:
        return Response(
            "Could not retriev 'data' contents", status = 400
        )

def handle_file_return(file_name, request):
    """
    opens the file and prepares the response
    """
    headers = Headers()
    headers.add('Content-Disposition', 'attachment', filename=file_name)

    return Response(
        wrap_file(request.environ, open(file_name, 'rb')),
        mimetype='application/pdf',
        headers=headers,
        status = 200
    )



def parse_request_data_post( request ):
    """
    retrieves the content data using different strategies
    """
    data = None
    options = json.loads(request.form.get('options', "{}" ))

    if 'data' in request.form:
        data = base64_decode( request.form['data'] )
    elif request.content_type.endswith('json'):
        # If a JSON payload is there, all data is in the payload
        payload = json.loads(request.data)
        data = str(payload['contents'].decode('base64'))
        if payload.get('options', None) :
            payload_options = payload.get('options', options)
            options = payload_options
    elif request.files:
        data =request.files['file'].read()

    if is_url( data ):
        # download url data
        data = fetch_data( data )

    return {
            "data": data,
            "options": options
    }

def fetch_data( url ):
    """
    downloads, opens and returns contents
    """

    import httplib2
    h = httplib2.Http(".cache")
    resp, content = h.request(url, "GET")
    return content


def is_url( data ):
    """
    tests if data is a valid url
    """
    parsed = parse.urlparse( str(data))
    has_scheme = parsed.scheme != None and parsed.scheme != ''
    has_netloc = parsed.netloc != None and parsed.netloc != ''
    return has_scheme and has_netloc


def create_pdf( data, name = 'download.pdf', options = {}):
    """
    creates the pdf using wkhtmltopdf
    """
    if not isinstance(data, (bytes, bytearray)):
        data = data.encode('utf-8')
    with tempfile.NamedTemporaryFile(suffix='.html') as source_file:
        source_file.write(data)
        source_file.flush()

        # Evaluate argument to run with subprocess
        args = ['wkhtmltopdf',"--load-error-handling", "ignore", "--load-media-error-handling", "ignore"]
        # Add Global Options
        if options:
            for option, value in options.items():
                args.append('--%s' % option)
                if value:
                    args.append('"%s"' % value)

        # Add source file name and output file name
        file_name = source_file.name
        args += [file_name, name]

        # Execute the command using executor
        execute(' '.join(args))

        return name


def base64_decode( encoded ):
    """
    decode a base64 string
    """

    try:
        decoded = base64.b64decode( encoded.encode('utf-8') )
        return decoded
    except binascii.Error:
        print('not base64')
        return encoded

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple(
        '127.0.0.1', 5000, application, use_debugger=True, use_reloader=True
    )
